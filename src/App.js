import React, { Component } from 'react';
import {BrowserRouter as Router , Link , Route, Switch} from 'react-router-dom';
import './App.css';
import ProductList from './productList';
import AddProduct from './addProduct';
import ProductDetails from './productDetails';



class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">
      <Switch>
        <Route exact path = "/"  component = {ProductList} />
        <Route exact path = "/addProduct"  component = {AddProduct} />
        <Route path = "/productDetail"  component = {ProductDetails} />
      </Switch> 
      </div>
    </Router>
    );
  }
}

export default App;

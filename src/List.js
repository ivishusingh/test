import React, { Component } from 'react';
import { connect } from 'react-redux';

class Lists extends Component {
    render() {
        console.log(this.props);
        return (
            <div>
                <button type="button" onClick={this.props.showProducts}>Show List Of Products</button>

                {
                    this.props.allProducts.map(
                        (item) => (
                            <div>
                                <ul>
                                    <li>{item.pname}</li>
                                    <li>{item.pprice}</li>

                                </ul>
                            </div>
                        )
                    )
                }
                <button type="button" onClick={() => this.props.history.push('/AddProduct')}>Add Product</button>
            </div>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        allProducts: state.products
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        showProducts: () => dispatch({ type: 'SHOWPRODUCTS' })
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Lists);
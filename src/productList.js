import React, { Component } from "react";
import { connect } from "react-redux";
import { Link, Route } from "react-router-dom";
import AddProduct from "./addProduct";
import "./App.css";
class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: this.props.productData,
      data: this.props.productData
    };
  }

  filterValue = e => {
    const data = this.state.data.filter(item =>
      item.Name.toLowerCase().includes(e.target.value.toLowerCase())
    );
    this.setState({ Data: data });
  };

  render() {
    console.log("list", this.props.productData);
    return (
      <div className="list">
        <Link to="/addProduct">
          <button>AddProducts</button> <br />
          <br />
        </Link>
        <Route path="/addProduct" component={AddProduct} />
        <input
          onChange={this.filterValue}
          placeholder="Search Product here -->"
        />{" "}
        <br />
        <br />
        {this.state.Data.map((item, index) => (
          <ul key={index}>
            <li> Name : {item.Name}</li>
            <li>Price : {item.Price}</li>
            <li>Tags : {item.Tags}</li>
            <li>
              <button onClick={() => this.props.delete(index)}>
                DeleteProduct
              </button>
            </li>
          </ul>
        ))}
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    productData: state.productData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    delete: index => dispatch({ type: "DELETE", value: index })
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductList);

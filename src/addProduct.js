import React, { Component } from "react";
import "./App.css";
import { connect } from "react-redux";
import ProductList from "./productList";
import { Link, Route } from "react-router-dom";

class AddProduct extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="App">
        <pre>
          ProductName :{" "}
          <input onChange={this.props.name} placeholder="enter ProductName" />{" "}
          <br />
          <br />
          Price :{" "}
          <input
            onChange={this.props.price}
            placeholder="enter Product price"
          />{" "}
          <br />
          <br />
          Tags :{" "}
          <input
            onChange={this.props.tags}
            value={this.props.Tag}
            placeholder="Enter Tags"
          />{" "}
          <button onClick={this.props.addTags}>AddTags</button>
          <br />
          <br />
          tags : [
          {this.props.multipleTag.map((item, index) => (
            <li key={index}>{item}</li>
          ))}{" "}
          ]
          <br />
          <br />
          <Link to="/">
            <button onClick={this.props.addProduct}>AddProduct</button>
          </Link>
        </pre>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    multipleTag: state.multipleTag,
    Tag: state.tags,
    name: state.name,
    price: state.price
  };
};

const mapDispatchToProps = dispatch => {
  return {
    name: e => dispatch({ type: "NAME", value: e.target.value }),
    price: e => dispatch({ type: "PRICE", value: e.target.value }),
    tags: e => dispatch({ type: "TAGS", value: e.target.value }),
    addTags: () => dispatch({ type: "ADD_TAGS" }),
    addProduct: () => dispatch({ type: "ADD_PRODUCT" })
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddProduct);

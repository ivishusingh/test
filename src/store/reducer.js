const initialState = {
  productData: [],
  name: "",
  price: "",
  tags: "",
  multipleTag: []
};

const reducer = (state = initialState, action) => {
  let newState = { ...state };

  switch (action.type) {
    case "NAME":
      newState.name = action.value;
      return newState;

    case "PRICE":
      newState.price = action.value;
      return newState;

    case "TAGS":
      newState.tags = action.value;
      return newState;

    case "ADD_TAGS":
      console.log("tags", newState.tags);
      newState.multipleTag = newState.multipleTag.concat(newState.tags);
      newState.tags = "";
      return newState;

    case "ADD_PRODUCT":
      newState.productData = newState.productData.concat({
        Name: newState.name,
        Price: newState.price,
        Tags: newState.multipleTag
      });

      console.log(newState.productData);
      return newState;

    case "DELETE":
      let data = [...newState.productData];
      data.splice(action.value, 1);
      newState.productData = data;

      return newState;
  }

  return newState;
};

export default reducer;
